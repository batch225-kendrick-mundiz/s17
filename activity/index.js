/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
function getUserInfo(){
	alert("Hi! Please add your information");
	let fullName = prompt("Enter your full name:"); 
	let age = prompt("Enter your age:"); 
	let address = prompt("Where do you live:");
    alert("Thank you for your input!");


	console.log("Your full name is :"+fullName); 
	console.log("You are "+age+" years old"); 
	console.log("You live in "+address); 
};


	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
function getTopBands(){
	let band1 = "Metallica";
	let band2 = "Bowling for Soup";
	let band3 = "Megadeth";
	let band4 = "Nirvana";
	let band5 = "Good Charlotte";


	console.log("1: "+band1); 
	console.log("2: "+band2); 
	console.log("3: "+band3); 
	console.log("4: "+band4); 
	console.log("5: "+band5); 
	 
};



	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	function getTopMovies(){
	let movie1 = "Top Gun";
	let movie2 = "Forrest Gump";
	let movie3 = "LOTR: Return of the King";
	let movie4 = "LOTR: The Two Towers";
	let movie5 = "LOTR: The Fellowship of the Ring";


	console.log("1: "+movie1); 
    console.log("Rotten tomato rating: 83%");
	console.log("2: "+movie2); 
    console.log("Rotten tomato rating: 95%");
	console.log("3: "+movie3); 
    console.log("Rotten tomato rating: 93%");
	console.log("4: "+movie4); 
    console.log("Rotten tomato rating: 95%");
	console.log("5: "+movie5); 
    console.log("Rotten tomato rating: 91%");
	 
};


	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

getUserInfo();
getTopBands();
getTopMovies();
printUsers();

